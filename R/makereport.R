


makereport <- function(src=NULL, title=NULL, author=NULL, open=TRUE,
                       header1="###", header2="##"){
  
    
 if(is.null(author)){
   if(!exists(".author")){
      .author <- readline("Who are you: ")
      assign(".author", .author, .GlobalEnv)
   }
 } else .author <- author

 if(is.null(src)){
   if(!exists(".src")){
     stop("Please provide name of source file (or set .src)")
   }
 } else { 
   .src <- src
   assign(".src",.src, .GlobalEnv)
 }
 if(is.null(title))title <- .src
 
 r <- c()
 r[1] <- title
 r[2] <- "================================"
 r[3] <- paste0("*",.author,"* - ", "`r format(Sys.time(),'%Y-%m-%d %H:%M')`")
 r[4] <- ""
 r[5] <- ""
 r[6] <- ""
 
 srcl <- readLines(.src)
 
 
 # Find header string
 
 insertHeader <- function(vec, hstr, mdtxt, beginline=TRUE, replace=FALSE){
   
   if(beginline)hstr <- paste0("^",hstr)
   
   if(any(grepl(hstr, vec))){
     H <- grep(hstr, vec)
     while(length(H) > 0){
       
       H <- H[1]
       
       if(!replace){
         # Get rid of header string
         vec[H] <- str_trim(gsub(hstr,"",vec[H]))
         ins <- c("```","",vec[H],mdtxt,"","```{r}")
       
       } else{
         
        ins <- c("```","",mdtxt,"","```{r}")
       }
       
       vec <- Replace(vec, H, ins)
       H <- grep(hstr, vec)  
     }
  }
 return(vec)
 }
 
 srcl <- insertHeader(srcl, header1, "==========")
 srcl <- insertHeader(srcl, header2, "------------")
 srcl <- insertHeader(srcl, "#-+#", "------------", replace=TRUE)

 
 # delete first few blank lines, if any
 if(srcl[1] == "")
  srcl <- srcl[-c(1:(min(which(srcl != ""))-1))]
 
 r <- c(r, srcl)
 r <- c(r, "")
 
 n <- length(r)
 r[length(r)] <- "```"
 r[6] <- "```{r}"
 
 
 dat <- format(Sys.time(),'%Y-%m-%d_%Hh-%Mm')
 
 src_noext <- gsub("[.].{1,3}$", "", .src)
 
 outf <- paste0(src_noext, "_", dat, ".html")
 
 writeLines(r, "tmp.Rmd")
 knit2html("tmp.Rmd", envir=new.env())
 
 file.rename("tmp.html",outf)
 unlink("tmp.Rmd")
 
 if(open)browseURL(outf)
    
}
